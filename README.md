# UE4 Code Module Generator

Simple command-line tool to add new code modules to your UE4 projects.

[The official documentation](https://docs.unrealengine.com/en-us/Programming/Modules/Gameplay) is sadly slightly outdated and not very detailed. Additionally, there is no automated way to create new code modules from within the editor / engine. As the setup of a new code module is somewhat tedious and repetitive, this tool aims to provide a slim and simple-to-use command-line tool to add new code modules to your UE4 projects.

The internals of this tool are inspired by [this kantan blog post](http://kantandev.com/articles/ue4-code-modules). For reasons why to split code into multiple modules, read that blog post.

## Compatibility

- Built with Visual Studio 2017 Community Edition
- Tested with Unreal Engine 4.19
- Tested on Windows 10

## Installation

*You can skip steps 1 and 2, if you downloaded the ModuleGenerator.exe directly*

1. Clone this repository
2. Build the project in Visual Studio
3. Copy the ModuleGenerator.exe from `bin/Debug` or `bin/Release` (depending on your selected build target) to your UE4 project's root (right next to your uproject file)

## Usage

1. Double-click the ModuleGenerator.exe (or execute it from within a command-line / powershell)
2. Enter the desired module name
3. Specify whether to automatically register the module in your uproject file
4. ???
5. Profit!

## Roadmap

If there is enough demand, the following additions are planned (no particular order):

- Add support to add code modules to plugins, in addition to games
- Add option to automatically register the module in either a Build.cs or Target.cs file
- Add option to generate example code/classes inside the new module with `TODO` comments, as pointers on how to start using the new module
- Add command-line parameters to skip interactive elements, for better integration into other scripts or automations
- Add management options to rename or delete existing code modules
- Maybe add dependency management options to add/remove module dependencies between various modules
- Convert this tool to a UE4-Plugin for the marketplace, so code modules could be generated out of the editor UI